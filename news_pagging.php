<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>News yang di pagging</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>

<body>
<div id="wrapper">
  <div id="header">
        
           <div class="container">
                <img id="logo" src="images/logo.png">
                <div id="menu">
                    <ul>
                        <li class="nav1"><a href="index.php">HOME</a></li>
                        <li class="nav2"><a href="news.php">NEWS</a></li>
                        <li class="nav3"><a href="products.php">PRODUCTS</a></li>
                        <li class="nav4"><a href="contact.php">CONTACT</a></li> 
                        <li class="nav5"><a href="gallery.php">GALLERY</a></li>
                    </ul>
                </div>
           </div>
            
     </div>
   <!---------------------------------------- END HEADER -------------------------------->
   <div id="greenLine"></div>
        <div id="content">
            
            <div class="container">

                <?php
                    /*1. pertama harus terhubung koneksi ke database seperti script di bawah*/
                    $koneksi = new mysqli("localhost","root","","progress_bisnis_db_ariyanto");

                    /*2. Buat Variabel untuk membatasi jumlah data*/
                    $view = 3;

                    /* 3.Tentukan sebuah halaman apakah ada parameter page atau tidak*/
                    if (isset($_GET['page']))
                    {   
                        /*jika ada halaman yang aktif  diawali dengan parameter ?page maka (di bawah ini)*/
                        $page_aktif = $_GET['page'];
                    } else {

                        /*jika tidak ada halaman yang aktif adalah berada di halaman ke 1 */
                        $page_aktif = 1;
                    }

                    /* 4. Tentukan awal data dari halaman yang aktif */ 
                    $awaldata  = ($page_aktif-1)*$view;

                    /*5. perintah SQL untuk menampilkan data dari data keberapa dan berapa jumlah yang di tampilakn */
                    $sql = "SELECT * FROM news_tbl LIMIT $awaldata,$view";

                    $query = $koneksi->query($sql);
                    $rownews = $query->fetch_assoc();

                    do{

                ?>


                <div class="newsitem">
                    <div class="date_circle"><p class="day">28<span class="month">06</span></p></div>
                    <h2 class="news_title"><?php echo $rownews['title']; ?></h2>
                    <div class="clear"></div>
                <div class="img_box" style="width: 221px; height: 182px; float: left;">
                    <img src="news_images/<?php echo $rownews['images']; ?>" class="news_image">
                </div>
                    <p class="news_synopsis"><?php echo substr($rownews['description'],0,250); ?></p>
                    <a href="news_detail.php?id_news=<?php echo $rownews['id_news']; ?>" class="link_detail">Read More</a>
                </div>
                
                <?php } while($rownews = $query->fetch_assoc()); ?>

                <?php 
                /*6. Tentukan jumlah data yang sebenarnya */

                $sqltotal = "SELECT * FROM news_tbl";
                $qtotal = $koneksi->query($sqltotal);
                $total_data = $qtotal->num_rows;

                /* 7. Berapa halaman yang di dapat */

                $total_page = ceil($total_data/$view);


                /* 8. Tampilkan link jumlah halaman dengan looping */
                 ?>
            <div class="pembungkusnomor" style="padding-top: 10px; height: 30px; ">
                <?php for ($i=1; $i<=$total_page; $i++) { ?>

                    <!-- Jika start $i / = page yang aktif -->
                    <?php if($i == $page_aktif) { ?>

                        <span class="pagelinkactive">
                            <?php echo $i; ?>
                            
                        </span>

                         <!-- End start $i / = page yang aktif -->

                         <!-- Page Link untuk kehalaman yang lainnya -->
                    <?php } else {  ?>
                        <a class="pagelink" href="?page=<?php echo $i; ?>">
                            <?php echo $i; ?>
                        </a>
                    <?php } ?>
                    <!-- End Page Link untuk kehalaman yang lainnya -->
                <?php } ?>
            </div>
            <style>
                .pagelinkactive {background-color: #60b000; color: #fff; text-align: center; height: 20px; width: 25px; display: block; float: left; border-radius: 3px; margin-right: 5px;}

                .pagelink {background-color: #000; color: #fff; text-align: center; height: 20px; width: 25px; display: block; float: left; border-radius: 3px; margin-right: 5px; text-decoration: none;}

                .pagelink:hover {background-color: #60b000;}
            </style>
         
            </div><!--- END CONTENT WRAPPER -->
                
        </div>
<!---------------------------------------- END CONTENT ------------------------------->
        <div id="footer">
        
            <div class="container">
                <p> Copyright &copy; Your Company All Right Reserved</p>
            </div>
        
        </div>
<!---------------------------------------- END FOOTER --------------------------------->  
</div>
</body>
</html>
